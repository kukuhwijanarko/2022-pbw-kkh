TEST<br>
<?php
//print 'Hello KKH<br>';
//print 1 + 4 ;
//Comment 1
# Comment 2
/*Multi Comment */

echo ('IPB');  //fungsinya sama seperti print
    print print ('IPB');//echo tidak mengembalikan nilai
    echo 'satu', 'dua', 'tiga';//echo bisa multi argument
    
//Variable//
$x = 1;
$x = 'a';
$x = 1.5;
$y = '5';

echo $x * 2;
print $x + $y;
echo $x.$y;    //concade

//Conditional//

$bil = 1;

if ($bil % 2 ) {
    echo '<br>ganjil';
} 
else {
    echo '<br>genap';
}

echo $bil % 2 ? 'ganjil' : 'genap';

switch ($bil % 2) {
    case 1:
        echo '<br>ganjil';
        break;
    default:
        echo '<br>genap';
}

//Loop//
for ($i = 1; $i <= 3; $i++) {
    echo '<br>'.$i;
}

$i = 4;
while ($i <= 3) {
    echo '<br>'.$i++;
}

do {
    echo '<br>'.$i++;
} while ($i <= 3);


$data = ['satu', 'dua', 'tiga'];

foreach ($data as $i) {
    echo '<br>'.$i;
}
?>